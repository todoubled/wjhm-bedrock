/**
 * File sticky-navigation.js.
 *
 * Required to transition the "sticky" class on scroll for the site header.
 */
const throttle = require('lodash/throttle');

( function() {
	/**
	 * Sticky Navigation : Runs on document loaded
	 */
  document.addEventListener("DOMContentLoaded", function(){
    // use "sticky" class based on header height
    const checkHeader = throttle((winTop, header, headerHeight) => { 
      if (winTop >= headerHeight){
        header.classList.add('sticky');
      } else {
        header.classList.remove('sticky');
      }//if-else
    }, 300);
  
    // Improve scroll performance on mobile devices by using the passive event listener option
    // ref: https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener
    let passiveSupported = false;
  
    // safely handle browser support for passive event handling
    try {
      const options = {
        get passive() { // This function will be called when the browser
                        //   attempts to access the passive property.
          passiveSupported = true;
          return false;
        }
      };
  
      // sticky-header-animated (Inspired by: https://coder-coder.com/sticky-header-animated/)
      window.addEventListener('scroll', function() {
        var winTop = window.scrollY;
        var header = document.querySelector('.site-header');
        checkHeader(winTop, header, header.offsetHeight);
      }, options);
    } catch(err) {
      passiveSupported = false;
    }
  });
}() );

//

