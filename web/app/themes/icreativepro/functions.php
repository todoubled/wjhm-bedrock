<?php
/** 
 * Child Theme Functions & Definitions
 * 
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * 
 * */
use Roots\WPConfig\Config;


if ( ! function_exists( 'twentytwentyone_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
  function twentytwentyone_setup() {
    $defaults = array(
      'height'      => 100,
      'width'       => 400,
      'flex-height' => true,
      'flex-width'  => true,
      'header-text' => array( 'site-title', 'site-description' ),
      'unlink-homepage-logo' => true, 
      );
    add_theme_support( 'custom-logo', $defaults );
    set_post_thumbnail_size( 9999, 250 );    
  }
endif;
add_action( 'after_setup_theme', 'twentytwentyone_setup', 12 );

/**
 * Enqueue scripts and styles.
 */
function child_theme_enqueue_scripts() {
    $parenthandle = 'twentytwentyone';
    $theme = wp_get_theme();

    // Load parent style (first)
    wp_enqueue_style( $parenthandle, get_template_directory_uri() . '/style.css', 
        array(),  // if the parent theme code has a dependency, copy it to here
        $theme->parent()->get('Version')
    );

    // load child style (last)
    wp_enqueue_style( 'twentytwentyone-child', get_stylesheet_uri(),
        array( $parenthandle ),
        $theme->get('Version') // this only works if you have Version in the style header
    );

    // wp_enqueue_style( 'twentytwentyone-child-dark', get_stylesheet_directory_uri() . '/assets/css/style-dark-mode.css',
    //     array( $parenthandle ),
    //     $theme->get('Version') // this only works if you have Version in the style header
    // );

    // load fonts
    // wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Lato:100,100italic,300,300italic,regular,italic,700,700italic,900,900italic|Overpass:100,100italic,200,200italic,300,300italic,regular,italic,600,600italic,700,700italic,800,800italic,900,900italic|Red+Hat+Text:regular,italic,500,500italic,700,700italic|DM+Serif+Text:regular,italic|Blinker:100,200,300,regular,600,700,800,900|Aleo:300,300italic,regular,italic,700,700italic|Nunito:200,200italic,300,300italic,regular,italic,600,600italic,700,700italic,800,800italic,900,900italic|Knewave:regular|Palanquin:100,200,300,regular,500,600,700|Palanquin+Dark:regular,500,600,700|Roboto:100,100italic,300,300italic,regular,italic,500,500italic,700,700italic,900,900italic|Oswald:200,300,regular,500,600,700|Oxygen:300,regular,700|Playfair+Display:regular,italic,700,700italic,900,900italic|Fira+Sans:100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic|Abril+Fatface:regular|Comfortaa:300,regular,500,600,700|Kaushan+Script:regular|Noto+Serif:regular,italic,700,700italic|Montserrat:100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic&subset=arabic,bengali,cyrillic,cyrillic-ext,devanagari,greek,greek-ext,gujarati,hebrew,khmer,korean,latin-ext,tamil,telugu,thai,vietnamese',
    //     array( $parenthandle ),
    //     $theme->get('Version') // this only works if you have Version in the style header
    // );

    // disable the title+tagline
    if (!get_theme_mod('display_title_and_tagline', false)) {
      set_theme_mod('display_title_and_tagline', false);
    }

    // Main navigation scripts.
	// if ( get_theme_mod( 'use_sticky_navigation' ) ) {
    wp_enqueue_script( 'sticky-navigation',
        get_stylesheet_directory_uri() . '/build/js/nav.js',
        array(),
        wp_get_theme()->get( 'Version' ),
        true
    );
	// }

}
add_action( 'wp_enqueue_scripts', 'child_theme_enqueue_scripts' );

/**
 * Dequeue the Parent Theme styles.
 *
 * Hooked to the wp_print_scripts action, with a late priority (100),
 * so that it is after the script was enqueued.
 */
function child_theme_dequeue_script() {
  wp_dequeue_style( 'twenty-twenty-one-style' );
  wp_deregister_style( 'twenty-twenty-one-style' );

  wp_dequeue_script( 'twenty-twenty-one-print-style' );
  wp_deregister_script( 'twenty-twenty-one-print-style' );
}
add_action('wp_enqueue_scripts','child_theme_dequeue_script', 100);

/**
 * Block Patterns.
 */
// require get_stylesheet_directory_uri() . '/inc/block-patterns.php';

// allow for easier styling on the hoe page w/sticky posts
add_filter(
	'body_class',
	function( $classes ) {
		$stickies = get_option( 'sticky_posts' );

		if ( count( $stickies ) && is_home() ) {
			return array_merge( $classes, array( 'has-sticky-post' ) );
		}

		return $classes;
	}
);

/* WP Head */
function ns_google_analytics() {
    if ( defined( 'GA_ANALYTICS_ID' ) ) {
        $google_analytics_id = Config::get('GA_ANALYTICS_ID');
        $gtag = sprintf("https://www.googletagmanager.com/gtag/js?id=%s", $google_analytics_id);
?>
	<script async src="<?php echo $gtag ?>"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', '<?php echo $google_analytics_id ?>');
		</script>
	<?php
    }
}

add_action( 'wp_head', 'ns_google_analytics', 10 );

/**
 * Clean up the head (https://gist.github.com/blacksaildivision/d74b3f92faf7f8a8b3a7d88cf7cd713e)
 * Opinionated list of clean-up (@TODO: Create a mu-plugin):
 * - Strip of rss feeds, resource hints, emojis, disable xml-rpc, style attributes
 *
 * DNS-Prefetch : third-party services that might need to be included for performance.
 */

/**
 * Alter dns-prefetch links in <head>
 */
add_filter('wp_resource_hints', function (array $urls, string $relation): array {
    // If the relation is different than dns-prefetch, leave the URLs intact
    if ($relation !== 'dns-prefetch') {
        return $urls;
    }

    // Remove s.w.org entry
    $urls = array_filter($urls, function (string $url): bool {
        return strpos($url, 's.w.org') === false;
    });

    // List of domains to prefetch:
    // Google fonts example: [ 'fonts.googleapis.com', ]
    $dnsPrefetchUrls = [ 'cdnjs.cloudflare.com','fonts.googleapis.com', 'fonts.googleapis.com', ];
    return array_merge($urls, $dnsPrefetchUrls);
}, 10, 2);

/**
 * Disable RSS feeds by redirecting their URLs to homepage
 */
foreach (['do_feed_rss2', 'do_feed_rss2_comments'] as $feedAction) {
    add_action($feedAction, function (): void {
        // Redirect permanently to homepage
        wp_redirect(home_url(), 301);
        exit;
    }, 1);
}

/**
 * Remove the feed links from <head>
 */
remove_action('wp_head', 'feed_links', 2);

/**
 * Remove emoji script and styles from <head>
 */
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

/**
 * Disable REST-API for all users except of admin
 */
add_filter('rest_authentication_errors', function ($access) {
    if (!current_user_can('administrator')) {
        return new WP_Error('rest_cannot_access', 'Only authenticated users can access the REST API.', ['status' => rest_authorization_required_code()]);
    }
    return $access;
});

/**
 * Remove REST-AI link from <head>
 */
remove_action('wp_head', 'rest_output_link_wp_head');

/**
 * Disable XML-RPC
 */
add_filter('xmlrpc_enabled', function (): bool {
    return false;
});

/**
 * Remove XML-RPC link from <head>
 */
remove_action('wp_head', 'rsd_link');

/**
 * Remove Windows Live Writer manifest from <head>
 */
remove_action('wp_head', 'wlwmanifest_link');

/**
 * Remove info about WordPress version from <head>
 */
remove_action('wp_head', 'wp_generator');

/**
 * Remove unnecessary attributes from style tags
 */
add_filter('style_loader_tag', function (string $tag, string $handle): string {
    // Remove ID attribute
    $tag = str_replace("id='${handle}-css'", '', $tag);

    // Remove type attribute
    $tag = str_replace(" type='text/css'", '', $tag);

    // Change ' to " in attributes:
    $tag = str_replace('\'', '"', $tag);

    // Remove trailing slash
    $tag = str_replace(' />', '>', $tag);

    // Remove double spaces
    return str_replace('  ', '', $tag);
}, 10, 2);
