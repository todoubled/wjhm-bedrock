<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('post-card'); ?>>
  <header class="entry-header">
    <figure class="post-thumbnail">
      <a class="post-thumbnail-inner" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
        <?php the_post_thumbnail( 'medium_large' ); ?>
      </a>
      <?php if ( wp_get_attachment_caption( get_post_thumbnail_id() ) ) : ?>
        <figcaption class="wp-caption-text"><?php echo wp_kses_post( wp_get_attachment_caption( get_post_thumbnail_id() ) ); ?></figcaption>
      <?php endif; ?>
    </figure>    
    <a href="<?php the_permalink(); ?>" class="entry-title--link">
      <?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
    </a>
  </header>

</article><!-- #post-<?php the_ID(); ?> -->
